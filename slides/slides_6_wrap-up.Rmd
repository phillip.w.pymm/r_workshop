---
title: '\small Introduction to "R"\newline\normalsize Concluding Remarks'
author: "Arndt Leininger\\newline\\newline \\faTwitter \\href{https://twitter.com/a_leininger}{@a\\_leininger} \\newline \\Email \\href{mailto:arndt.leininger@fu-berlin.de}{arndt.leininger@fu-berlin.de}"
date: "Hertie School, Feb 27, 2021"
output:
  beamer_presentation:
    keep_tex: true
    toc: false
    colortheme: "seahorse"
    fonttheme: "structurebold"
    fig_caption: false
    includes:
      in_header: header.tex
---

```{r setup, include=FALSE}
# setwd('~/Git/r_workshop/slides')
options(width=55)
knitr::opts_chunk$set(echo = TRUE, eval = FALSE, cache = FALSE,
                      tidy=TRUE, tidy.opts=list(width.cutoff=55))
```

# Course evaluation

## Course evaluation

- Please take the time and participate in a short evaluation of the PDC "Introduction to R"
- The evaluation is available on [MyStudies](https://mystudies.hertie-school.org/en/course-evaluation/evaluate.php) (-> go to Evaluations -> scroll down to the respective course and click on the 'Evaluate' link)
- Please fill in the questionnaire and click on 'submit evaluation' in the end.

![](img/course_evaluation.png)

# Using R and RStudio outside the cloud

## Installing packages

- Packages that are not shipped with R need to be installed before you can load and use them
- `install.packages()` takes a package name as input and installs the requisite package

```{r}
install.packages('dplyr')
install.packages('tidyr')
```

or

```{r}
install.packages(c('dplyr', 'tidyr'))
```

- Package names must be passed on as a character vector

## Loading packages

- Before you can use the functions provided by the new packages you need to load them

```{r library3, eval=TRUE, error=TRUE}
library(A3)
```
```{r library4, eval=FALSE}
install.packages('A3')
library(A3)
```

- Install only once (while updating occasionally)
- Call `library()` every time you work in a new R session

## Duplicate function names

Sometimes two different functions have the same name

```{r lag_example, eval=TRUE}
lag(c(1,2,3))

stats::lag(c(1,2,3))
```

## ::

Using `[package]::[function]` you can use a function without first loading the package with `library`

```{r, eval=TRUE}
stringr::str_replace(c('Hello', 'World', '!'), '!', '.')
```

<!--
## *Beware of `require()`*

- Don't use `require()` which does not provide an error message if a package could not be loaded

```{r require, eval=TRUE, error=TRUE}
require('thispackagedoesnotexit')
library('thispackagedoesnotexit')
```

https://yihui.name/en/2014/07/library-vs-require/
-->

## *Installing development versions*

```{r devtools}
install.packages('devtools')

devtools::install_github('bookdown')
```

## Working directory

- The working directory is the default directory in which R looks for files
- You did not have to worry about this in RStudio Cloud but you will need to understand the concept when working on your local machine

## `getwd()`

If you're unsure what your working directory is set to:

```{r}
getwd()
```

```{r, eval=TRUE, echo=FALSE}
setwd('~/Git/r_workshop')
getwd()
```

## File paths

- File paths tell the computer where to read and write information.
- Separators between folder names differ between Windows (`\`) and Mac/Linux (`/`).
- However, in R, independent of the OS used, forward slashes (`/`) are used exclusively.

## Absolute and relative file paths on Windows

```
C:
+- Users
+- Bill
|  +- Dropbox
|     +- coolproject
|        + code
|        + data
|          +- master.csv
+- Melinda
```

- Absolute path to project folder: `C:/Users/Bill/Dropbox/coolproject`
- Relativ path to file `master.csv`: `data/master.csv`

## Absolute and relative file paths on Mac OS

```
/
+- Users
+- Steve
|  +- Dropbox
|     +- coolproject
|        + code
|        + data
|          +- master.csv
+- Laurene
```

- Absolute path to folder `coolproject`: `/Users/Steve/Dropbox/coolproject`
- Relative path to file `master.csv`: `data/master.csv`

## Absolute and relative file paths on Linux

```
/
+- home
+- linus
|  +- Dropbox
|     +- coolproject
|        + code
|        + data
|          +- master.csv
+- tove
```

- Absolute path to project folder: `/home/linus/Dropbox/coolproject`
- Relative path to file script.R: `data/master.csv`

## Absolute vs. relative paths

- Ideally use an absolute path only for setting the working directory
- Everything else should be relative paths
- Absolute paths cause errors when you give a project folder to a coauthor/collaborator

```{r}
setwd('C:/Users/Bill/Dropbox/coolproject') # code for Bill
setwd('/Users/Steve/Dropbox/coolproject') # code for Steve
setwd('/home/linus/Dropbox/coolproject') # code for Linus

df <- read.csv('data/master.csv') # same relative path for everyone
```

<!--
## *Directory structure*

- *Absolute paths* start from the top of the tree and specify each
subdirectory until the file e.g. `C:\Users\Bill\document.docx` (or
`/home/linus/document.tex`, or even `https://gitlab.com/arndtl/r_workshop`)
- However in R the paths are `C:/Users/Bill/document.docx` and `/home/linus/document.tex`
- *Relative paths* start from the current working directory; use `../` to move to the directory above the current one
- R starts with your home directory as working directory.
- Say you have your Stats II stuff in `C:\User\JDoe\Dropbox\stats2` then to set this folder as working directory you use `setwd('C:/User/JDoe/Dropbox/Hertie/stats2')`
-->

<!--
## *Converting \\ to /*

```{r converting_slashes}
gsub("\\\\",  "/",  readClipboard())
```
-->

<!--
## Opening a dataset

```
/
+- home
   +- arndt
      +- Git
         +- r_workshop
            +- data
               +- BundestagForecastReplicationData.csv
```

```{r}
# 1. Set your working directory
setwd('/home/arndt/Git/r_workshop/')
```

```{r read_csv}
# 2. open the file
d <- read.csv('data/BundestagForecastReplicationData.csv')
```
-->

## Session > Set Working Directory

- Instead of copying and pasting  file paths from Explorer or Finder (and correcting slashes)
- You can also use RStudio'S GUI: Session > Set Working Directory > ...
    - To source files location
    - To file pane location
    - Choose Directory (\keys{ctrl} + \keys{shift} + \keys{h})
- RStudio calls `setwd()` in the console for you
- You can copy and paste that code into your script

## Projects in RStudio

- Instead of setting the working directory in your code you can also use projects in RStudio
- Projects allow you to
    - permanently set a working directory
    - save your `workspace`
    - save a `history` of executed code
    - save setting to RStudio GUI
    - mange Git through RStudio
    - start RStudio by clicking on your `*.proj` file
- These things are saved in the subfolder `.Rproj.user`
- To avoid problems with syncing services such as Dropbox exclude this folder form syncing

## Projects in RStudio

- Use
    - File > New Project
    - File > Open Project
- See also: https://support.rstudio.com/hc/en-us/articles/200526207-Using-Projects

# Hands-on-6-1

## Hands-on-6-1

Access the project "Hands-on-6-1" in the space "Introduction to R" on RStudio Cloud **and download the scripts to your computer to use your local installation of RStudio!**

**Create a new folder `hands-on-6-1` and save the script inside this folder.**

- `hands-on-6-1.R` contains the tasks, write your answers into the scripts
- If you're stuck, try whether you can fill in the blanks and fix the errors in `hands-on-6-1-partial.R` 
- `hands-on-6-1-solutions.R` contains the full solutions

# What's next

## This course

will remain online and is also available for offline use at:

![](img/gitlab.png)

https://gitlab.com/arndtl/r_workshop

## Take home-exercise Part 2

- Available on RStudio Cloud (make sure to copy to your own space)
- In course materials (provided via Gitlab) in folder `exercise`: `R_exercise_2_questions.pdf`

Apply what you learned today and try to apply some new functions.

*NB: Please understand that the course ends here and I am not able to respond to questions on the second part of the exercise.*

## Further learning

<!--
[Try R](https://www.codeschool.com/courses/try-r) An interactive R tutorial by Code School.

[Introduction to R](https://www.datacamp.com/courses/free-introduction-to-r) An interactive online course by DataCamp. The course is free but requires registration.
-->

- [Beginner's guide to R](http://www.computerworld.com/article/2497143/business-intelligence/business-intelligence-beginner-s-guide-to-r-introduction.html) A five-articles series on R by Computerworld.
- [RStudio Primers](https://rstudio.cloud/learn/primers)
    - Also available on the left-hand side navigation in RStudio Cloud
- [GitHub Student Developer Pack](https://education.github.com/pack#offers)
    - Hertie students can use their school email address to get the Github Student Developer Pack to access various services for free

<!--
## Features to look at

- [Multiple cursors and other shortcuts](https://www.karambelkar.info/2017/01/rstudio-ide-shortcuts-learned-at-rstudioconf-2017/)
    - \keys{\shift + \Alt + K}
    - `Tools` $\rightarrow$ `Modify Keyboard Shortcuts`
- [RStudio projects](https://support.rstudio.com/hc/en-us/articles/200526207-Using-Projects) 

# Packages to look at

## *tidyverse*

Also check out:

- `readxl` for reading Excel sheets
- `stringr` for working with text and so-called regular expressions
- `lubridate` for working dates and times

## *Statistical modelling*

- `zoo` for time-series
- `panel` for panel and tscs data
- `lme4` for multilevel models

- `broom` for turning model objects into `data.frame`s for visualization

- `modelsummary`, `textreg`, `stargarzer` for regression tables
-->