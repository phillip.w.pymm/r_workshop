# Introduction to "R"
# Introduction to R and RStudio
#
# Hands-on 1-3

# 1 Open the file BundestagForecastReplicationData.csv and assign its content to
# an object.
d <- read.csv('BundestagForecastReplicationData.csv')

# 2 How many observations are in the dataset?
nrow(d)

# 3 How many variables does the dataset contain?
ncol(d)

# 4 Create a variable of type logical which is TRUE for each election after 1990
# and FALSE for all others. Simply checke whether year is greater or equal to
# 1990 and write the result of that comparison in a new variable.
d$post1990 <- d$year >= 1990

# 5 Calculate the mean of the variable outgovshare.
mean(d$outgovshare, na.rm = T)

# 6 Remove the logical variable indicating the post-1990 elections.
d$post1990 <- NULL

# 7 Z-Tranform the variable outgovshare. The function sd() calculates the
# standard deviation of a variable which you need for z-transformation.
# Remember: z = (x - mean(x))/sd(x)
df$outgovshare <- (df$outgovshare - mean(df$outgovshare, na.rm = T)) /
  sd(df$outgovshare, na.rm = T)

# 8 Print the variable chancellor. There should be a problem with the umlaut in
# Gerhard Schröder's name. Correct it by replacing the 15th and 16h values in the
# variable chancellor with "Gerhard Schröder".

d$chancellor[c(15, 16)] <- 'Gerhard Schröder' # this results in an error message

# Google it to understand why!

# Here's one explanation: https://statisticsglobe.com/warning-message-invalid-factor-level-na-generated-in-r

# The problem, in short is, that read.csv turns colums with characters
# (i.e., strings) into factor variables by default. A factor variable has a
# fixed set of values that it can take on. You can't just add new values.
# Trying to write in an undefined value results in NAs. You can either
# (temporarily) turn the variable into a character variable or you add the
# following argument to read.csv: stringsAsFactors

# 9 Save the data in a separate file.
write.csv(d, 'BundestagForecastReplicationData_edit.csv', row.names = F)