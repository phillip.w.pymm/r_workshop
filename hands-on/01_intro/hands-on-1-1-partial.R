# Introduction to "R"
# Introduction to R and RStudio
#
# Hands-on 1-1

# This script contains the solutions to the exercise but the code contains
# deliberate omissions (denoted by __) and errors. You have fill in or fix them
# to make the code run through.

# 1. Add two and four and divide by 3
(2 + 4) / __

# 2. Perform this calculation again and save it to an object called a
a __ (2 + 4) / 6

# 3. Calculate the result of the division of 23/7 and save it to an object b
b <- __

# 4. print the content of b to the console
print(__)  # or simply

# 5. substract object a from object b
b - __

# 6. Save the result of b - a in b
__ __ b - a

# 7. Use rnorm() to create 10 random draws from a standard normal
# Remember that you can use the 'tab' key to obtain tooltips for the function
rnorm(__ = 10)

# 8. Call up the help file for the function rnorm(). What happens if you run
# rnorm(10, 2, 1)?

help(__) # or
?rnorm

rnorm(10, 2 1) # creates 10 random draws from a normal distribution
# with mean 2 and standard deviation 1

# 9. Save the result of rnorm(10, 2, 1) in an object called result and then, in
# a second step, substract 1 from object result. What do you notice?

result <- rnorm(10, 2, 0
result - 1

# 10. Try different pane layouts and editor themes to find your preferred setup.
# Tools > Global Options

# Some mathematical expressions in R
# 2 + 2 # Addition
# ## [1] 4
#
# 3 - 2 # Substraction
# ## [1] 1
#
# 2 * 2 # Multiplication
# ## [1] 4
#
# 10 / 3 # Division
# ## [1] 3.333333
#
# 10 %% 3 # Remainder
# ## [1] 1
#
# 10 %/% 3 # Quotient
# ## [1] 3