# Introduction to "R"
# Model estimation
#
# Hands-on 4-1

# This script contains the solutions to the exercise but the code contains
# deliberate omissions (denoted by __) and errors. You have fill in or fix them
# to make the code run through.

# 1. Read the dataset "EUsuppDK.dta" and assign it to an object. You'll need the
# function read.dta() from the package foreign.
library(__)
d <- readdta('data/EUsuppDK.dta')

# 2. Regress eusupp on gender, wealth and age.
lm(eusupp __ sex __ wealth __ age, d)

# 3. Interpret the coefficients. For this obtain the summary() of the model results.
lm(eusupp ~ sex + wealth + age, _) %__% summary()

# 4. Obtain the predicted values for the following model: eusupp ~ sex + wealth + age.
# Save them to an object yhats.
summary(m <- lm(eusupp ~ sex + wealth + age, d))
yhats __ predict(m)
Yhats

# 5. Then also create a variable containing the predicted values in the original
# data.frame. Hint: simply assing the object yhats won't work.
d$yhats <- predict(m, __ = d)

# 6. If you use the model to predict eusupp. How far off are your predictions
# on average?
average(abs(resid(m)))

# 7. How supportive of the EU do you expect a very rich 50 year old woman to be
# based on your regression model? Hint: Construct a new dataset using the
# data.frame() function and feed it to lm using the data argument.
df <- data.frame(sex = __, wealth = '__', age = __)

predict(m, newdata = __)
